using System;
    public class Account
    {
        private decimal _balance;
        private string _name;

        public Account(string name, decimal startingBalance)
        {
            _name= name;
            _balance = startingBalance;
        }
        public bool Deposit( decimal amountToAdd)
        {
            if (amountToAdd > 0)
            {
                _balance = _balance + amountToAdd;
                return true;
            }
            return false;
            
        }
        public bool Withdraw( decimal amounttominus)
        {
            if (amounttominus > 0)
            {
                _balance = _balance - amounttominus;
                return true;
            }
            return false;
            
        }
        public String name 
        {
            get {return _name; }

        }
        public decimal balance
        {
            get { return _balance; }

        }
        public void Print()
        {
            Console.WriteLine(name);
            Console.WriteLine(balance);
        }
    }
