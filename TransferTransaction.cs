using System;

public class TransferTransaction
{
    private Account _toAccount;
    private Account _fromAccount;
    private decimal _amount;
    private DepositTransaction _theDeposit;
    private WithdrawTransaction _theWithdraw;
    private bool _executed = false;
    private bool _reversed = false;

    public TransferTransaction(Account fromAccount, Account toAccount, decimal amount)
    {
        _toAccount = toAccount;
        _fromAccount = fromAccount;
        _amount = amount;
        _theWithdraw = new WithdrawTransaction(_fromAccount, _amount);
        _theDeposit = new DepositTransaction(_toAccount, _amount);
    }
    
    public bool Success
    {
        get
        {
            if (_theDeposit.Success && _theWithdraw.Success)
            {
                return true;
            } else {
                return false;
            }
        }
    }

    public bool Executed
    {
        get
        {
            return _executed;
        }
    }

    public bool Reversed
    {
        get
        {
            return _reversed;
        }
    }

    public void Execute()
    {
        if ( _executed )
        {
            throw new Exception("Cannot execute this transaction as it has already been executed.");
        }

        _theWithdraw.Execute();
        if (_theWithdraw.Success)
        {
            _theDeposit.Execute();
            if (!(_theDeposit.Success))
            {
                _theWithdraw.Rollback();
            }
        }
    }

    public void Rollback()
    {
        if ( ! ( _executed ) )
        {
            throw new Exception("This transaction has not been executed. It cannot be reversed.");
        }

        if ( _reversed )
        {
            throw new Exception("This transaction has already been reversed.");
        }

        _reversed = true;
        if (_theDeposit.Success)
        {
            _theDeposit.Rollback();
        }
        if (_theWithdraw.Success)
        {
            _theWithdraw.Rollback();
        }
    }

    public void Print()
    {
        if (Success)
        {
            Console.WriteLine("Transferred $" + _amount + " from " + _fromAccount.name + " to " + _toAccount.name);
            Console.WriteLine("\n");
            _theWithdraw.Print();
            Console.WriteLine("\n");
            _theDeposit.Print();
        } else {
            Console.WriteLine("Transfer Failed");
        }
        

    }
}