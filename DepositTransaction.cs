using System;

public class DepositTransaction
{
    private Account _account;
    private decimal _amount;
    private bool _executed = false;
    private bool _success = false;
    private bool _reversed = false;

    public bool Success
    {
        get
        {
            return _success;
        }
    }

    public bool Executed
    {
        get
        {
            return _executed;
        }
    }

    public bool Reversed
    {
        get
        {
            return _reversed;
        }
    }

    public DepositTransaction(Account account, decimal amount)
    {
        _account = account;
        _amount = amount;
    }

    public void Execute()
    {
        if ( _executed )
        {
            throw new Exception("Cannot execute this transaction as it has already been executed.");
        }

        _executed = true;
        _success = _account.Deposit(_amount);
    }

    public void Rollback()
    {
        if ( ! ( _executed ) )
        {
            throw new Exception("This transaction has not been executed. It cannot be reversed.");
        }

        if ( _reversed )
        {
            throw new Exception("This transaction has already been reversed.");
        }

        _reversed = true;
        _account.Withdraw(_amount);
    }

    public void Print()
    {
        if ( _success )
        {
            Console.WriteLine("Transaction was successful.");
            Console.WriteLine("Amount Deposited: " + _amount);

            if ( _reversed )
            {
                Console.WriteLine("This transaction was reversed.");
            }
        } else {
            Console.WriteLine("Transaction Failed.");
        }
    }
}