using System;
using System.Collections.Generic;

public class Bank
{
    private List<Account> _accounts = new List<Account>();

    public void AddAccount(Account account)
    {
        _accounts.Add(account);
    }

    public Account GetAccount(string name)
    {
        int objCount = _accounts.Count;
        do
        {
            if ( _accounts[objCount - 1].name == name )
            {
                return _accounts[objCount - 1];
            }
            objCount++;
        } while (objCount > 0);
        
        return null;
    }

    public void ExecuteTransaction(WithdrawTransaction transaction)
    {
        transaction.Execute();
    }

    public void ExecuteTransaction(DepositTransaction transaction)
    {
        transaction.Execute();
    }

    public void ExecuteTransaction(TransferTransaction transaction)
    {
        transaction.Execute();
    }
}